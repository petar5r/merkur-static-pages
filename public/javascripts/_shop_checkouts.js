(function ($) {

  $(document).on('click', '.delivery-day > span', function (e) {
    e.preventDefault();
    var $this = $(this);
    if ($(this).hasClass('opened')) {
      $(this).removeClass('opened');
      $(this).closest('.delivery-wrap').find('.toggle-element__wrapper').animate({ height: 0 }, 300, function () {
        $this.find('.fa').addClass('fa-plus-square').removeClass('fa-minus-square');
      });
    }
    else {
      $(this).addClass('opened');
      var innerWrapHeight = $(this).closest('.delivery-wrap').find('.pricing-boxes__wrapper').outerHeight(true);
      $(this).closest('.delivery-wrap').find('.toggle-element__wrapper').animate({ height: innerWrapHeight }, 300, function () {
        $this.find('.fa').addClass('fa-minus-square').removeClass('fa-plus-square');
      });
    }
  });

  $(document).on('click', '.product-amount__value button', function (e) {
    e.preventDefault();
    var currValue = parseInt($(this).closest('.product-amount__value').find('input').val(), 10);
    var dataMin = parseInt($(this).closest('.product-amount__value').find('input').attr('data-minval'), 10);
    var dataMax = parseInt($(this).closest('.product-amount__value').find('input').attr('data-maxval'), 10);
    var amountToSet;
    if (!$(this).hasClass('checkout-amount_button-disabled')) {
      if ($(this).hasClass('value_amount-up')) {
        amountToSet = currValue + 1;
        $(this).closest('.product-amount__value').find('.checkout-amount_button-disabled').removeClass('checkout-amount_button-disabled');
        $(this).closest('.product-amount__value').find('input').val(amountToSet);
      }
      if ($(this).hasClass('value_amount-down')) {
        amountToSet = currValue - 1;
        $(this).closest('.product-amount__value').find('.checkout-amount_button-disabled').removeClass('checkout-amount_button-disabled');
        $(this).closest('.product-amount__value').find('input').val(amountToSet);
      }
      if (amountToSet <= dataMin) { $(this).addClass('checkout-amount_button-disabled'); }
      if (amountToSet >= dataMax) { $(this).addClass('checkout-amount_button-disabled'); }
    }
  });

  $(document).on('click', '.checkout__single-product .checkout__remove-product', function (e) {
    e.preventDefault();
    $(this).closest('.checkout__single-product').animate({ height: 0 }, 250, function () {
      $(this).remove();
    });
  });

  $(document).on('click', '.checkout__products-header a.checkout__accordion-trigger', function (e) {
    e.preventDefault();
    var $this = $(this);
    if ($(this).hasClass('checkout__products--opened')) {
      var totalHeight = $(this).closest('.checkout__products').find('.checkout__products-list__inner').outerHeight(true);
      $(this).closest('.checkout__products').find('.checkout__products-list').css({'overflow' : 'visible'}).animate({ height: totalHeight }, 300, function () {
        $this.removeClass('checkout__products--opened').children('.fa').addClass('fa-minus-square').removeClass('fa-plus-square');
      });
    }
    else {
      $(this).closest('.checkout__products').find('.checkout__products-list').css({'overflow' : 'hidden'}).animate({ height: 0 }, 300, function () {
        $this.addClass('checkout__products--opened').children('.fa').addClass('fa-plus-square').removeClass('fa-minus-square');
      });
    }
  });

  $(document).on('click', '.payment-selection__wrapper input[type="radio"]', function () {
    if (!$(this).closest('.payment-selection__element').hasClass('payment-selection--selected')) {
      $(this).closest('.payment-selection__section').find('.payment-selection--selected').removeClass('payment-selection--selected');
      $(this).closest('.payment-selection__element').addClass('payment-selection--selected');
    }
  });

  $(document).on('click', '[class*="pricing-box__wrap"]', function(){
    if($(this).find('.no-delivery').length === 0){
      $(this).closest('.container').find('.delivery-selected').removeClass('delivery-selected');
      $(this).find('.pricing-box__single').addClass('delivery-selected');
    }
  });



  $(document).on('click', '.info-button', function(){
    if(!$(this).hasClass('popup--shown')){
      $(this).find('.info-button__info-popup').show().animate({opacity : 1});
      $(this).addClass('popup--shown');
    }
  });
  $(document).on('click', '.info-button .popup_close', function(){
    $(this).closest('.info-button').find('.info-button__info-popup').animate({opacity: 0},function(){$(this).hide(); $(this).closest('.info-button').removeClass('popup--shown');});

  });






})(jQuery);
