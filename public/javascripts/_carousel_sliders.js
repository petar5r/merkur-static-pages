// init slider carousel
var initCarousel = function () {
  $('.carousel-slider').each(function (index) {

    var items = $(this).data("init-items");
    var responsiveOptions;

    switch (items) {
      case 2: responsiveOptions = {
                0: { items: 1, slideBy: 1 },
                560: { items: 2, slideBy: 2 }
              };
              break;
      case 5: responsiveOptions = {
                0: { items: 1, slideBy: 1 },
                381: { items: 2, slideBy: 2 },
                600: { items: 3, slideBy: 3 },
                1000: { items: 4, slideBy: 4 },
                1250: { items: 5, slideBy: 5 }
              };
              break;
      default: responsiveOptions = {
                0: { items: 1, slideBy: 1 },
                381: { items: 2, slideBy: 2 },
                600: { items: 3, slideBy: 3 },
                1000: { items: 4, slideBy: 4 },
                1250: { items: 5, slideBy: 5 }
              };
    }

    $(this).owlCarousel({
      loop: true,
      lazyLoad: true,
      margin: 0,
      responsiveClass: true,
      responsive: responsiveOptions,
      nav: true,
      navText: [
          // prev slide button
          "<a class='carousel-slider__btn carousel-slider__btn--prev' href='javascript:;' role='button'>" +
              "<span class='sr-only'>Zur&uuml;ck</span>" +
          "</a>",
          // next slide button
          "<a class='carousel-slider__btn carousel-slider__btn--next' href='javascript:;' role='button'>" +
              "<span class='sr-only'>Weiter</span>" +
          "</a>"
      ]
    });
  });
};
