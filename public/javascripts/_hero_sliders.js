﻿// init hero sliders
var initHeroSlider = function () {
  $(".hero-slider").layerSlider({
    pauseOnHover: true,
    autoPlayVideos: false,
    skinsPath: heroSliderSkinPath,
    skin: 'noskin'
  });
};
