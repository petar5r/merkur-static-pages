﻿//handle isotope gridlayout
var initGridLayout = function () {

  var $container = $('.grid-layout__inner');
  // init
  $container.isotope({
    // options
    itemSelector: '.grid-layout__item',
    layoutMode: 'fitRows'
  });

  // toggle brands on click
  $(".brand-toggler").click(function () {
    var brands = $(this).nextAll(".item__nav");

    if ($(brands).hasClass("open")) {
      $(this).removeClass("open");
      $(brands).slideUp("fast").removeClass("open");
    }
    else {
      $(".brand-list-item").find(".brand-toggler").removeClass("open");
      $(".brand-list-item").find(".item__nav").slideUp("fast").removeClass("open");
      $(this).addClass("open");
      $(brands).slideDown("fast").addClass("open");
    }
  });

};

