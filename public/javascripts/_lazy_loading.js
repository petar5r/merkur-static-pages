﻿// init lazy image loading
var initLazyLoading = function () {
  echo.init({
    offsetVertical: 300,
    throttle: 250,
    unload: false,
    callback: function (element, op) { }
  });

  // echo.render(); is also available for non-scroll callbacks
};
