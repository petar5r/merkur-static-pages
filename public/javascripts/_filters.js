﻿//handle filters
var handleFilters = function () {

  // add class collapsed, when showing additional filters
  if ($(".anchor-collapse").length > 0) {
    $('.anchor-collapse').click(function () {
      if (!$(this).hasClass("collapsed")) {
        $(this).addClass("collapsed");
      }
      else {
        $(this).removeClass("collapsed");
      }
    });
  }

  // filter lists
  var blockActiveFilters = $("#blockActiveFilters");
  var activeFilters = $('#activeFilters');

  // remove filter from list
  if ($('.js-remove-filter').length > 0) {
    $('.js-remove-filter').each(function () {
      $(this).click(function () {
        // remove filter from list
        $(this).parent().remove();

        // hide filter block if no filter is active
        if ($(activeFilters).children().length == 0) {
          $(blockActiveFilters).hide();
        }
      });
    });
  }
};
