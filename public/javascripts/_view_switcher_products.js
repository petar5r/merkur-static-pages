﻿//view switcher on product overview
var handleProductViewSwitcher = function () {
  var miniView = $('.js-switch-miniview');
  var defaultView = $('.js-switch-defaultview');

  // switch to mini view
  $(miniView).click(function () {
    //--> load mini view

    if (!$(this).hasClass("active")) {
      $(this).addClass("active");
      $(defaultView).removeClass("active");
    }

    // show quick shopping notification
    handleNotification($(this));
  });

  // switch to default view
  $('.js-switch-defaultview').click(function () {
    //--> load default view

    if (!$(this).hasClass("active")) {
      $(this).addClass("active");
      $(miniView).removeClass("active");
    }
  });


  // mini view - show cart button onclick
  if ($('.js-show-action').length > 0) {
    $('.js-show-action').each(function () {
      $(this).click(function () {
        $(this).parent().hide();
        $(this).parent().next(".product-item__action").show();
      });
    });
  }
};
