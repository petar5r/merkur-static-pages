//js code
$(document).ready(function(){

	/* Navigation for Desktop version */

	// Hover for main menu items
	$('[data-menu-item]').hover(function() {
		$menuLi = $(this);
		$menuItem = $menuLi.data('menu-item');
		$menuBlock = $('*[data-item="'+$menuItem+'"]');

		// Removing class active from all links in navigation and adding class for hovered link
		$('.main-navigation__item a').removeClass('active');
		$('a',$menuLi).addClass('active');

		// Hiding if some subnavigation item is shown
		$('[data-item]').hide(0,function(){

			$('*[data-item="'+$menuItem+'"]').show(0,function(){
				$('.megamenu').stop(true, true).fadeIn(50); // Showind megamenu after content inside is loaded
				$('.megamenu__block', $menuBlock).css({'min-height': $('*[data-item="'+$menuItem+'"]').height()+'px'}) // setting height of right block so ve have border from top to bottom
				$('.megamenu-content-hover').stop(true, true).fadeIn(50); // Showing overlay over website when menu ili shown
			});
		})
	}, function() {
		$menuLeave = $('*[data-menu-item="'+$menuItem+'"]');
		// If mouse is over smenu we keep it open
		if ($('[data-item]:hover').length > 0){
			return false;
		}else{
			// We are hiding menu if mouse is out
			hideMenu()
		}
		$('*[data-item="'+$menuItem+'"]').mouseleave(function() {
			// If mouse is not over submenu we are checking if it is over main menu
			// if is there we are not hiding it, elde submenu will be hidded
			if ($('[data-menu-item]:hover').length > 0){
				return false;
			}else{
				hideMenu()
			}	
		});
}
);

	// We have delay for closing navigation 800 miliseconds, if maus goes out and back to subnavigation we keep it open by faking mouseenter on main nav item
	$('[data-item]').mouseenter(function(){
		$('*[data-menu-item="'+$(this).data('item')+'"]').mouseenter()
	});




  //Mobile menu trigger
        $('#nav-expander').on('click',function(e){
      		e.preventDefault();

      		$('.website__hoder ').addClass('open');
      		$('#mobile__navigation ').addClass('open');
      		$('.megamenu-content-hover').stop(true, true).fadeIn(50);
      	});

      	// Mobile menu close
      	$('#nav-close').on('click',function(e){
      		e.preventDefault();
      		$('.website__hoder').removeClass('open');
      		$('#mobile__navigation ').removeClass('open');
      		$('.megamenu-content-hover').stop(true, true).fadeOut(50);
      	});


 

	
});

// Function for hiding megamenu on desktop
function hideMenu(){
	$('.megamenu').stop(true, true).delay(800).fadeOut(400)
	$('.megamenu-content-hover').stop(true, true).delay(800).fadeOut(400);
	$('.main-navigation__item a').removeClass('active')

}

// Function for mobile menu subitems show/hide
function submenuShow($submenu, $this){
	if($('*[data-submenu="'+$submenu+'"]').hasClass('opened')){
		$('.has__submenu').removeClass('opened')
		$('*[data-submenu="'+$submenu+'"]').removeClass('opened');
	}else{
			$('.has__submenu').removeClass('opened')
		$($this).addClass('opened')
		$('.submenu').removeClass('opened')
		$('*[data-submenu="'+$submenu+'"]').addClass('opened');
}
}
