﻿//handles notifications

var handleNotification = function (obj) {
  var type = $(obj).data('notification-type');
  var title = $(obj).data('notification-title');
  var message = $(obj).data('notification-message');

  if (title.trim().length > 0 || message.trim().length > 0) {
    toastr.options = {
      "closeButton": true,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-bottom-right",
      "preventDuplicates": true,
      "onclick": null,
      "showDuration": "500",
      "hideDuration": "500",
      "timeOut": "5000",
      "extendedTimeOut": "5000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    };

    switch (type) {
      case 'info': toastr.info(message, title); break;
      case 'success': toastr.success(message, title); break;
      case 'warning': toastr.warning(message, title);
        break;
      case 'error': toastr.error(message, title);
        break;
      default: toastr.info(message, title);
    }
  }
};
