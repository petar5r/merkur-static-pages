// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require turbolinks
//= require ./_carousel_sliders
//= require ./_grid_layouts
//= require ./_hero_sliders
//= require ./_notifications
//= require ./_shop_checkouts
//= require ./_page_scroller
//= require ./_filters
//= require ./_lazy_loading
//= require ./_tab_container
//= require ./_view_switcher_products
//= require ./_detail_zoom

var heroSliderSkinPath = 'assets/stylesheets/vendor/layerslider-skin/';

$(function () {

  //handle page scroller
 // handlePageScroller();

  //init lazy image loading
 // initLazyLoading();

  //tab containers
  if ($(".nav-tabs").length > 0) {
    initTabCollapse();
  }

  // init carousel slider
  // dependencies: [jquery, owl.carousel.min.js, _carousel_sliders.js]
  if ($('.carousel-slider').length > 0) {
    initCarousel();
  }

  // init grid layout
  // dependencies: [jquery, isotope.pkgd.min.js, _grid_layouts.js]
  if ($('.grid-layout').length > 0) {
    initGridLayout();
  }

  // init hero slider
  if ($('.hero-slider').length > 0) {
    initHeroSlider();
  }

  // init zoom plugin product detail
  if ($('.swinxydock').length > 0) {
    initZoomDetail();
  }

  // handle filters - products/categories
  // dependencies: [jquery]
  if ($('.filter-wrapper').length > 0) {
    handleFilters();
  }

  // handle view switcher - product overview
  // dependencies: [jquery]
  if ($('.view-switcher--products').length > 0) {
    handleProductViewSwitcher();
  }

  // handle view switcher - recipes
  // dependencies: [jquery]
  if ($('.view-switcher--recipes').length > 0) {
    handleRecipeViewSwitcher();
  }


  $( ".tab-toogle a" ).click(function() {
    $( ".tab-toogle a span" ).toggle();
    $("#myTabContent").toggle();
  });





});
