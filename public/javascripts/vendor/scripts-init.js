(function($){

	$(document).on('show.bs.tab', '#checkout_wizard a[data-toggle="tab"]', function(e, index){
		var $target = $(e.target);
		var $targetInd = $target.closest('li').index();
		$target.closest('ul').find('li').each(function(){
			if($(this).index() < $targetInd){
				$(this).addClass('wizard_active--element');
			}
			else{
				$(this).removeClass('wizard_active--element');
			}
		});
	});

	$(window).load(function(){
	  $('#checkout_wizard').bootstrapWizard();
		$('#checkout_wizard .finish').click(function() {
			console.log('Finished!, Starting over!');
			$('#checkout_wizard').find("a[href*='tab1']").trigger('click');
		});

		$(document).on('click', '#checkout_wizard ul.wizard li button', function(e){
			e.preventDefault();
			$('body, html').animate({'scrollTop' : 0},300);
		});

	});

})(jQuery);
